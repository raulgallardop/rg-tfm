import _pickle as pickle
from LoadData import Environment
from LoadData import PositionEnv
from LoadData import Saver
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm
import glob
import sys


symbol = "010005SAP"

if len(sys.argv) > 1:
    print(sys.argv[1])
    symbol = sys.argv[1]
s = Saver()
env = s.Load("data", "environment-POV-" + symbol)
posenv = s.Load("data", "positionenvironment-POV-" + symbol)


LEARNING_RATE = 0.1
DISCOUNT = 0.95
cumulative_volume = 0
target_volume = 0
episode_reward = 0
INITIAL_POSITION = 100000

desv_list_agregated = []


for episode in tqdm(range(100)):
    permanent_shift_05 = permanent_shift_075 = permanent_shift_2 = 0
    i = np.random.randint(len(env.ask))
    first = env.getFirstAskPrice(i)
    total_cost = 0
    total_avg_price = 0
    done = False
    discrete_state = posenv.getDiscreteState()
    posenv.reset(INITIAL_POSITION)
    desv_list = []

    buy_list = []
    POV_list = []
    action_list = []
    posenv.POV_Prc
    price_list = []
    pov_price_list = []
    while not done and i < len(env.ask) - 1:

        if np.random.random() > 0.05:
            # Get action from Q table
            
            action = np.argmin(abs(posenv.getTabularAction(discrete_state)))
        else:
            # Get random action
            action = np.random.randint(0, env.num_action_space)

        action_list.append(action)
        quantity = posenv.step(action)

        posenv.updatePOVVolume(env.getVolume(i))
        cost, inventory, avg_price = env.getcost(quantity, i)

        cost_POV, inventory_POV, avg_price_POV = env.getcost(env.getVolume(i) * 0.2, i)
        posenv.liquidate(quantity - inventory)
        price_list.append((quantity - inventory, avg_price))
        buy_list.append(INITIAL_POSITION - posenv.getQuantity())
        POV_list.append(posenv.getPOVolume())
        if avg_price != -1:
            diff_price = (avg_price - env.getFirstAskPrice(i))
            permanent_shift_05 += diff_price * 0.5
            permanent_shift_075 += diff_price * 0.75
            permanent_shift_2 += (avg_price - env.getFirstAskPrice(i)) * 2
        pov_price_list.append((env.getVolume(i) * 0.2, avg_price_POV))

        total_cost += cost
        cumulative_volume += env.getVolume(i)
        
        i += 1
        if avg_price > -1:
            total_avg_price += avg_price * (quantity - inventory) / INITIAL_POSITION
        # we have updated the POV Volume and the liquidated inventory
        new_discrete_state = posenv.getDiscreteState()

        cost = posenv.getLiquidationState()
        desv_list.append(abs(posenv.getLiquidationState()))
        if not done:
            # Minimum possible Q value in next step (for new state)
            min_future_q = np.min(abs(posenv.getTabularAction(new_discrete_state)))
            
            current_q = posenv.getQValue(discrete_state, action)
            
            new_q = (1 - LEARNING_RATE) * current_q + LEARNING_RATE * (cost + DISCOUNT * min_future_q)

            # Update Q table with new Q value
            
            posenv.setQValue(discrete_state, action, new_q)

        
        if i == len(env.ask):
            done = True
            first = -1
            total_cost = -1
            total_avg_price = -1
            print("no hecho")
        if posenv.getQuantity() == 0:
            deviation = np.std(desv_list)
            auxDF = pd.DataFrame(pov_price_list, columns=["Volume", "Price"])

            pov_average = np.sum(auxDF["Price"] * auxDF["Volume"] / np.sum(auxDF["Volume"]))
            auxDF = pd.DataFrame(price_list, columns=["Volume", "Price"])
            auxDF = auxDF[auxDF["Price"]!= -1]
            price_average = np.sum(auxDF["Price"] * auxDF["Volume"] / INITIAL_POSITION)
            done = True
            desv_list_agregated.append((np.mean(desv_list),deviation, pov_average, price_average, \
                                        price_average + permanent_shift_05, price_average + permanent_shift_075, \
                                        price_average + permanent_shift_2))
            del auxDF
        discrete_state = new_discrete_state

np.savetxt('data/'+symbol+"-POV.csv", desv_list_agregated, delimiter=';')
print("----The End----")
