import pandas as pd
import glob
import sys
import os
import tqdm

symbol = "010005SAP"

if len(sys.argv) > 1:
    print(sys.argv[1])
    symbol = sys.argv[1]
FOLDER_PATH = "../AssetData/"+symbol+"/"
OUTPUT_FOLDER = "../AssetData/"+symbol+"-filtered/"
FILE_LIST = glob.glob(FOLDER_PATH + "*.csv")

if not os.path.isdir(OUTPUT_FOLDER):
    os.mkdir(OUTPUT_FOLDER)
    print("Creating folder: " + OUTPUT_FOLDER)


header = "TimeStamp;Time;Open;High;Low;Close;Volume;Bid1;Bid2;Bid3;Bid4;Bid5;Bid6;"
header += "Bid7;Bid8;Bid9;Bid10;Ask1;Ask2;Ask3;Ask4;Ask5;Ask6;Ask7;Ask8;Ask9;"
header += "Ask10;BidVol1;BidVol2;BidVol3;BidVol4;BidVol5;BidVol6;BidVol7;"
header += "BidVol8;BidVol9;BidVol10;AskVol1;AskVol2;AskVol3;AskVol4;AskVol5;"
header += "AskVol6;AskVol7;AskVol8;AskVol9;AskVol10;"
header += "BidOrders1;BidOrders2;BidOrders3;BidOrders4;"
header += "BidOrders5;BidOrders6;BidOrders7;BidOrders8;BidOrders9;BidOrders10;"
header += "AskOrders1;AskOrders2;AskOrders3;AskOrders4;AskOrders5;AskOrders6;"
header += "AskOrders7;AskOrders8;AskOrders9;AskOrders10"

counter = 0
for file_name in tqdm.tqdm(FILE_LIST):

    df = pd.read_csv(file_name, sep=';', header=0, names=header.split(";"))
    aux = df[df["Bid2"] != 2147483647]
    if len(aux) != 0:
        print(file_name)
        counter += 1
        aux.to_csv(OUTPUT_FOLDER + file_name.split(sep="/")[-1], header= None, sep=";", index=False)

print("Total files: ", counter)
