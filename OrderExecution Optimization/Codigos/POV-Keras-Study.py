import _pickle as pickle
from LoadData_Keras import Environment
from LoadData_Keras import PositionEnv
from LoadData_Keras import Saver
from LoadData_Keras import DQN
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys
import glob
from tqdm import tqdm

symbol = "010005SAP"

if len(sys.argv) > 1:
    print(sys.argv[1])
    symbol = sys.argv[1]
s = Saver()
env = s.Load("data", "environment-POV-Keras")
posenv = s.Load("data", "positionenvironment-POV-Keras")

DISCRETE_WIN_SIZE = 20
BASE_PATH = "../AssetData/" + symbol + "-clean/"
FILE_LIST = glob.glob(BASE_PATH + "*csv")
FILE_LIST.sort()
env = Environment(FILE_LIST, "", False, file_surname=symbol, size=DISCRETE_WIN_SIZE)

num_actions = 5
max_experiences = 10000
min_experiences = 100
batch_size = 32
lr = 1e-2
gamma = 0.95
num_features = 2

TrainNet = DQN(num_actions, num_features, gamma, max_experiences, min_experiences, batch_size, lr)
TargetNet = DQN(num_actions, num_features, gamma, max_experiences, min_experiences, batch_size, lr)
TrainNet.LoadModel("data/Train")
TargetNet.LoadModel("data/Target")

desv_list_agregated = []

for episode in tqdm(range(100)):
    cumulative_volume = 0
    target_volume = 0
    episode_reward = 0

    i = np.random.randint(len(env.ask))
    first = env.getFirstAskPrice(i)
    total_cost = 0
    total_avg_price = 0
    done = False
    INITIAL_POSITION = 100_000
    LEARNING_RATE = 0.1
    DISCOUNT = 0.95
    discrete_state = posenv.getDiscreteState()
    posenv.reset(INITIAL_POSITION)
    desv_list = []

    buy_list = []
    POV_list = []
    action_list = []
    price_list = []
    POV_price_list = []

    permanent_shift_05 = permanent_shift_075 = permanent_shift_2 = 0
    losses = list()
    iter = 0


    while not done and i < len(env.ask) - 1:
        liquidation = posenv.liquidation_state
        prc_liquidation = posenv.getQuantity() / INITIAL_POSITION
        s = [liquidation, prc_liquidation]
        action = TrainNet.get_action(s, 0.10)

        action_list.append(action)
        
        quantity = posenv.step(action)
        posenv.updatePOVVolume(env.getVolume(i))
        cost, inventory, avg_price = env.getcost(quantity, i)

        cost_POV, inventory_POV, avg_price_POV = env.getcost(env.getVolume(i) * 0.2, i)
        price_list.append((quantity - inventory, avg_price))
        buy_list.append(INITIAL_POSITION - posenv.getQuantity())
        POV_list.append(posenv.getPOVolume())

        if avg_price != -1:
            diff_price = (avg_price - env.getFirstAskPrice(i))
            permanent_shift_05 += diff_price * 0.5
            permanent_shift_075 += diff_price * 0.75
            permanent_shift_2 += (avg_price - env.getFirstAskPrice(i)) * 2

        POV_price_list.append((env.getVolume(i) * 0.2, avg_price_POV))

        posenv.liquidate(quantity - inventory)
        
        cumulative_volume += env.getVolume(i)
        
        i += 1
        iter += 1
        
        # we have updated the POV Volume and the liquidated inventory
        new_discrete_state = posenv.getDiscreteState()

        
        cost = posenv.getLiquidationState()

        desv_list.append(abs(posenv.getLiquidationState()))
        liquidation = posenv.liquidation_state
        prc_liquidation = posenv.getQuantity() / INITIAL_POSITION
        s2 = [liquidation, prc_liquidation]
        exp = {'s': s, 'a': action, 'r': abs(cost), 's2': s2}  # , 'done': done}
        TrainNet.add_experience(exp)
        loss = TrainNet.train(TargetNet)
        losses.append(loss)
        

        if i == len(env.ask):
            done = True
            first = -1
            total_cost = -1
            total_avg_price = -1
            print("No hecho")
        if posenv.getQuantity() == 0:
            
            auxDF = pd.DataFrame(POV_price_list, columns=["Volume", "Price"])

            pov_average = np.sum(auxDF["Price"] * auxDF["Volume"] / np.sum(auxDF["Volume"]))
            auxDF = pd.DataFrame(price_list, columns=["Volume", "Price"])
            auxDF = auxDF[auxDF["Price"] != -1]
            price_average = np.sum(auxDF["Price"] * auxDF["Volume"] / INITIAL_POSITION)
            done = True
            desv_list_agregated.append((np.mean(desv_list), np.std(desv_list), pov_average, price_average, \
                                        price_average + permanent_shift_05, price_average + permanent_shift_075, \
                                        price_average + permanent_shift_2))
            del auxDF
            done = True
        discrete_state = new_discrete_state

np.savetxt('data/'+symbol+"-POV-Keras.csv", desv_list_agregated, delimiter=';')
print("----The End----")
