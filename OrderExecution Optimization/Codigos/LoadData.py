import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import _pickle as pickle
import os

class Environment():
    def __init__(self, file_list=None, base_path='./', load_files = False, file_surname="", size = 20):
        if load_files:
            with open(f"data/askdata{file_surname}.pickle", "rb") as f:
                self.ask = pickle.load(f)
        else:
            if file_list is None:
                file_list = []
                for f in os.listdir(base_path):
                    if '.csv' in f:
                        file_list.append(f)
            askdata = pd.DataFrame()
            pricedata = pd.DataFrame()
            volumedata = pd.DataFrame()
            for i in file_list:
                print(i)
                
                askd, askp, vold = self.readDataFile(base_path + i, size)
                askdata = pd.concat([askdata, askd])
                pricedata = pd.concat([pricedata, askp])
                volumedata = pd.concat([volumedata, vold])
            askdata.reset_index(drop=True, inplace=True)
            pricedata.reset_index(drop=True, inplace=True)
            volumedata.reset_index(drop=True, inplace=True)
            self.ask = askdata.values
            self.price = pricedata.values
            self.volume = volumedata[0]
            self.time = self.setTime()

            if not os.path.isdir('data'):
                os.makedirs('data')
            f = open(f"data/askdata{file_surname}.pickle", "wb")
            pickle.dump(self.ask, f)
            
        self.num_action_space = 5
        print(f"The file len is {len(self.ask)}")


    def cleanBidAsk(self, data, minmove):
        data["Bid1"] = data["Bid2"] + minmove
        data["Ask1"] = data["Ask2"] - minmove
        data["Midprice"] = (data["Ask1"] + data["Bid1"]) / 2

        return data

    def mapTime(self, time):
        t = time.split(' ')[1]
        s = int(t.split(":")[0]) * 60 + int(time.split(":")[1])#time.replace(":","")
        return s
    def setTime(self):
        timeFunc = np.vectorize(self.mapTime)
        return timeFunc(self.price[:,0])
    def getAskData(self, data):
        return pd.concat([data["Time"],data.filter(regex="AskVol")], axis=1, sort=False)
    def getAskPrice(self, data):
        return pd.concat([data["Time"], data.filter(regex="Ask[^\D+]")], axis=1, sort=False)
    def getVolumeData(self, data):
        return data["DiffVol"]
    def getVolume(self, tick):
        return self.volume[tick]

    def readDataFile(self, filepath, size):
        header = "TimeStamp;Time;Open;High;Low;Close;Volume;Bid1;Bid2;Bid3;Bid4;Bid5;Bid6;"
        header += "Bid7;Bid8;Bid9;Bid10;Ask1;Ask2;Ask3;Ask4;Ask5;Ask6;Ask7;Ask8;Ask9;"
        header += "Ask10;BidVol1;BidVol2;BidVol3;BidVol4;BidVol5;BidVol6;BidVol7;"
        header += "BidVol8;BidVol9;BidVol10;AskVol1;AskVol2;AskVol3;AskVol4;AskVol5;"
        header += "AskVol6;AskVol7;AskVol8;AskVol9;AskVol10;"
        header += "BidOrders1;BidOrders2;BidOrders3;BidOrders4;"
        header += "BidOrders5;BidOrders6;BidOrders7;BidOrders8;BidOrders9;BidOrders10;"
        header += "AskOrders1;AskOrders2;AskOrders3;AskOrders4;AskOrders5;AskOrders6;"
        header += "AskOrders7;AskOrders8;AskOrders9;AskOrders10;Midprice;DiffVol"

        
        data = pd.read_csv(filepath, sep=';', header=0, names=header.split(";"), index_col=False)
        data.reset_index(drop=True, inplace=True)
        askd = self.getAskData(data)
        askp = self.getAskPrice(data)
        vold = self.getVolumeData(data)        
        return askd, askp, vold    

    def getTime(self, tick):
        return self.time[tick]
    def getFirstAskPosition(self, tick):
        return self.ask[tick][1]
    def getFirstAskPrice(self, tick):
        return self.price[tick][1]
    def getcost(self, liquidation, tick, ticksize = 0.005):
        cost = 0
        askposition = 1
        inventory = liquidation
        asklevel = self.ask[tick]
        avgprice = 0  #alf.price[tick][1]
        total_filled = 0
        while inventory > 0 and askposition <= 10:
            # volumen by level
            if inventory < asklevel[askposition]:
                # weighted average
                cost += inventory * (askposition-ticksize)
                avgprice += inventory * self.price[tick][askposition]
                total_filled += inventory
                inventory = 0
            else:
                cost += asklevel[askposition] * (askposition - ticksize)
                numcontracts = asklevel[askposition]
                inventory -= numcontracts
                avgprice += numcontracts * self.price[tick][askposition]
                total_filled += numcontracts
            askposition += 1
        av = -1
        if total_filled != 0:
            av = avgprice /total_filled
        return cost, inventory, av
class Position:
    def __init__(self, quantity):
        self.quantity = quantity

    def __str__(self):
        return f"The current inventory left is {self.quantity}"

    def __sub__(self, other):
        return self.quantity - other.quantity

    def __eq__(self, other):
        return self.quantity == other.quantity

    def action(self, choice):
        '''

        :param choice: the number of contract or shares to liquidate
        :return: the number of contracts left
        '''
        q = 0
        init = self.quantity
        #self.quantity -= choice
        if choice == 1:
            q = 5
        elif choice == 2:
            q = 100
        elif choice == 3:
            q = 500
        elif choice == 4:
            q = 1000 

        return min(init, q)
class PositionEnv:
    
    RETURN_IMAGES = True
    DISCRETE_WIN_SIZE = 20
    ACTION_SPACE_SIZE = 10
    POV_Prc = 0
    POV_volume = 0
    vwapFlag = False
    NUM_FEATURES = 2  # current stock to liquidate as pct and distance to the POV
    def __init__(self, InitialQuantity = 50000, POV_Prc = 20, VWAP = ""):
        self.POV_Prc = POV_Prc * 0.01
        self.POV_volume = 0
        self.InitialQuantity = InitialQuantity
        #self.q_table = np.random.uniform(low=0, high=10000, size=(DISCRETE_OS_SIZE + [10]))
        self.bucket_size = InitialQuantity / self.DISCRETE_WIN_SIZE
        
        if VWAP != "":
            self.NUM_FEATURES = 3 # Liquidation, difference between algo and model and time 
            self.setVWAP(VWAP)

        self.DISCRETE_OS_SIZE = [self.DISCRETE_WIN_SIZE] * (self.NUM_FEATURES)
        self.bins = self.createBinnedList()
        self.q_table = np.full(self.DISCRETE_OS_SIZE + [5], fill_value=10000)
        self.reset(InitialQuantity)
    def setVWAP(self, file=""):
        if file != "":
            self.vwap = pd.read_csv(file, delimiter=";", index_col=0)
            self.initial_time = self.vwap.index[0]
            self.end_time = self.vwap.index[-1]
            self.vwapFlag = True
    def reset(self, InitialQuantity):
        self.position = Position(InitialQuantity)
        self.liquidation_state = 0
        self.episode_steps = 0
        self.POV_volume = 0
       
    def getQuantity(self):
        return self.position.quantity
    def step(self, action):
        self.episode_steps += 1
        return self.position.action(action)
    def liquidate(self, quantity):
        self.position.quantity = self.position.quantity - quantity
    def updatePOVVolume(self, volume):
        self.POV_volume += volume * self.POV_Prc
    def  updateVWAPVolume(self, time):
        self.POV_volume += self.getVWAPTimeVolume(time) * self.POV_Prc
    def getVWAPTimeVolume(self, time):
        return self.vwap.loc[time][0]

    def getPOVolume(self):
        return self.POV_volume
    def getDiscreteState(self, t=0):

        #calculate liquidation state
        liquidation_ratio = (self.InitialQuantity - self.getQuantity()) / self.InitialQuantity
        pov_ratio = self.POV_volume / self.InitialQuantity
        self.liquidation_state = liquidation_ratio - pov_ratio
        liquidation_bin = np.digitize(self.liquidation_state, self.bins[0])

        # inventory state
        inventory_bin = np.digitize(self.getQuantity(), self.bins[1])
        
        if self.vwapFlag:
            vwap_bin = np.digitize(self.getTimePosition(t), self.bins[2])
            return (liquidation_bin, inventory_bin, vwap_bin)
        else:
            return (liquidation_bin, inventory_bin)
    

    def getLiquidationState(self):
        return self.liquidation_state
    def getTabularAction(self, index):
        return self.q_table[index]
    def getQValue(self, index, action):
        return self.q_table[index + (action,)]
    def setQValue(self, index, action, value):
        self.q_table[index + (action,)] = value
    def getTimePosition(self, time):
        return (time - self.initial_time) / self.end_time
    def createBinnedList(self):
        np.random.seed(10)
        # we modelize a random normal variable
        # the liquidation process may centered to 0 (no deviation)
        # so we make a random list to simulate it
        random_list = np.random.normal(0, 0.05, 1000)
        # we scale the limits of the variable. Normal variables are not bounded
        # we have made an small deviation and then we scale the limits
        # to -1 and 1. We set those limits because the algorithm
        # can not liquidate anything with (a deviation of -1)
        # or can liquidate all the stock with no volume (a limit of 1)
        random_list *= 1 / max(abs(random_list))
        out, bins = pd.qcut(random_list, self.DISCRETE_WIN_SIZE, retbins=True)
        bins = np.delete(bins, 0)
        bins[self.DISCRETE_WIN_SIZE - 1] = 1.00001

        # liquidated inventory bins
        bin_size = self.InitialQuantity / self.DISCRETE_WIN_SIZE
        bins2 = np.arange(bin_size, self.InitialQuantity + bin_size, bin_size)
        bins2[self.DISCRETE_WIN_SIZE - 1] += .000001
        
        if self.vwapFlag:
            bin_size = (self.end_time - self.initial_time) / self.DISCRETE_WIN_SIZE
            bins3 = np.arange(self.initial_time, self.end_time, bin_size)
            #bins = np.vstack([bins, bins2, bins3])
            bins3[self.DISCRETE_WIN_SIZE - 1] += .000001
            bins = np.vstack([bins, bins2, bins3])

        else:
            bins = np.vstack([bins, bins2])

        return bins

class Saver:

    def Save(self, object, path, name):
        with open(path+"/"+name+".pkl", 'wb') as output:
            pickle.dump(object, output)#, pickle.HIGHEST_PROTOCOL)
    def Load(self, path, name):
        with open(path+"/"+name+".pkl", 'rb') as input:
            object = pickle.load(input)
        return object
