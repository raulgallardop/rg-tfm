import pandas as pd
import numpy as np
import datetime
import glob
import sys
# polynomial regression
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split


symbol = "010005SAP"

if len(sys.argv) > 1:
    print(sys.argv[1])
    symbol = sys.argv[1]

INPUT_PATH = "../AssetData/" + symbol + "-clean/"
OUTPUT_PATH = "../AssetData/VWAP/" + symbol + ".csv"
FILE_LIST = glob.glob(INPUT_PATH + "*csv")
FILE_LIST.sort()

header = "TimeStamp;Time;Open;High;Low;Close;Volume;Bid1;Bid2;Bid3;Bid4;Bid5;Bid6;"
header += "Bid7;Bid8;Bid9;Bid10;Ask1;Ask2;Ask3;Ask4;Ask5;Ask6;Ask7;Ask8;Ask9;"
header += "Ask10;BidVol1;BidVol2;BidVol3;BidVol4;BidVol5;BidVol6;BidVol7;"
header += "BidVol8;BidVol9;BidVol10;AskVol1;AskVol2;AskVol3;AskVol4;AskVol5;"
header += "AskVol6;AskVol7;AskVol8;AskVol9;AskVol10;"
header += "BidOrders1;BidOrders2;BidOrders3;BidOrders4;"
header += "BidOrders5;BidOrders6;BidOrders7;BidOrders8;BidOrders9;BidOrders10;"
header += "AskOrders1;AskOrders2;AskOrders3;AskOrders4;AskOrders5;AskOrders6;"
header += "AskOrders7;AskOrders8;AskOrders9;AskOrders10;MidPrice;VolDiff"

dataraw = pd.DataFrame()
for i in FILE_LIST:
    print(i)
    dataraw = pd.concat([dataraw,pd.read_csv(i, sep=';', header=0, names=header.split(";"), index_col=False)])

data = dataraw[["Time", "VolDiff"]]
TimeMil = []
DateMil = []

for index, row in data.iterrows():
    time = row.Time.split()[1][:-3]

    DateMil.append(int(row.Time.split()[0].replace("-", "")))
    TimeMil.append(time)
data = data.assign(TimeMil = np.array(TimeMil), DateMil = np.array(DateMil))

# Clean outliers -> we set higher values to .999 as .999 value

data.loc[data['VolDiff'] >= data["VolDiff"].quantile(0.999), 'VolDiff'] = data["VolDiff"].quantile(0.999)
groupedData = data.groupby(['TimeMil','DateMil'], as_index=False)['VolDiff'].sum().groupby(["TimeMil"]).mean()

def mapTime(time):
    s = int(time.split(":")[0]) * 60 + int(time.split(":")[1])#time.replace(":","")
    return s

train, test = train_test_split(groupedData, test_size=0.3)
vfunc = np.vectorize(mapTime)

#Train
trainyData = train["VolDiff"].values
trainyData = trainyData[:, np.newaxis]
trainxData = train.index.values[:, np.newaxis]
trainxData = vfunc(trainxData)

#Test
testyData = test["VolDiff"].values
testyData = testyData[:, np.newaxis]
testxData = test.index.values[:, np.newaxis]
testxData = vfunc(testxData)

#Total
yData = groupedData["VolDiff"].values
yData = yData[:, np.newaxis]
xData = groupedData.index.values[:, np.newaxis]
xData = vfunc(xData)


polynomial_features= PolynomialFeatures(degree=2)
x_poly_train = polynomial_features.fit_transform(xData)
x_poly_test = polynomial_features.fit_transform(testxData)
x_poly_total = polynomial_features.fit_transform(xData)
model = LinearRegression()
model.fit(x_poly_train, yData)
y_poly_pred = model.predict(x_poly_test)

rmse = np.sqrt(mean_squared_error(testyData,y_poly_pred))
r2 = r2_score(testyData,y_poly_pred)
print("El mean square error es: ", rmse)
print("El r2 es: ", r2)

y_poly_pred = model.predict(x_poly_total)
output = pd.DataFrame(y_poly_pred, index=xData.flatten(), columns=["Volume"])
output.index.name = "Time"
#output.to_csv(OUTPUT_PATH, ';')
print("----End VWAP Data----")
