import numpy as np
import matplotlib.pyplot as plt
from LoadData import Environment
from LoadData import PositionEnv
from LoadData import Saver
import glob
from tqdm import tqdm
import sys
import os


symbol = "010005SAP"

if len(sys.argv) > 1:
    print(sys.argv[1])
    symbol = sys.argv[1]
BASE_PATH = "../AssetData/"+ symbol + "-clean/"
FILE_LIST = glob.glob(BASE_PATH + "*csv")
FILE_LIST.sort()
DISCRETE_WIN_SIZE = 20
INITIAL_POSITION = 100000
POV_PRC = 20


env = Environment(FILE_LIST, "", False, file_surname="-" + symbol, size=DISCRETE_WIN_SIZE)
s = Saver()

if os.path.isfile('data/positionenvironment-POV.pkl'):
    print("Load")
    posenv = s.Load("data", "positionenvironment-POV")
else:
    print("Not Load")
    posenv = PositionEnv(INITIAL_POSITION, POV_PRC)



LEARNING_RATE = 0.1
DISCOUNT = 0.95
EPISODES = 10000
SHOW_EVERY = 3000
STATS_EVERY = 100
DISCRETE_WIN_SIZE = 20


# Exploration settings
epsilon = 1  # not a constant, qoing to be decayed
START_EPSILON_DECAYING = 1
END_EPSILON_DECAYING = EPISODES//4
epsilon_decay_value = epsilon/(END_EPSILON_DECAYING - START_EPSILON_DECAYING)


desv_list_aggregated = []
for episode in tqdm(range(EPISODES)):
    cumulative_volume = 0
    target_volume = 0
    episode_reward = 0
    
    i = np.random.randint(len(env.ask))
    first = env.getFirstAskPrice(i)
    total_cost = 0
    total_avg_price = 0
    done = False

    discrete_state = posenv.getDiscreteState()
    posenv.reset(INITIAL_POSITION)
    desv_list = []
    epsilon -= epsilon_decay_value
    if episode % SHOW_EVERY == 0:
        render = True
        print(episode)
    else:
        render = False

    while not done and i < len(env.ask)-1:

        if np.random.random() > max(0.15, epsilon):
            # Get action from Q table
            action = np.argmin(abs(posenv.getTabularAction(discrete_state)))
        else:
            # Get random action
            action = np.random.randint(0, env.num_action_space)
       
        quantity = posenv.step(action)
        posenv.updatePOVVolume(env.getVolume(i))
        cost, inventory, avg_price = env.getcost(quantity, i)
        posenv.liquidate(quantity - inventory)
        
        total_cost += cost
        cumulative_volume += env.getVolume(i)
        
        i+=1
        if avg_price > -1:
            total_avg_price += avg_price * (quantity - inventory) / INITIAL_POSITION
        
        # we have updated the POV Volume and the liquidated inventory
        new_discrete_state = posenv.getDiscreteState()

        
        cost =posenv.getLiquidationState()
        desv_list.append(abs(posenv.getLiquidationState()))
        if action == 0:
            cost *= 1.1
        # If simulation did not end yet after last step - update Q table
        if not done:

            # Minimum possible Q value in next step (for new state)
            min_future_q = np.min(abs(posenv.getTabularAction(new_discrete_state)))
            # Current Q value (for current state and performed action)
            
            current_q = posenv.getQValue(discrete_state, action)
            # And here's our equation for a new Q value for current state and action
            new_q = (1 - LEARNING_RATE) * current_q + LEARNING_RATE * (cost + DISCOUNT * min_future_q)

            # Update Q table with new Q value
            posenv.setQValue(discrete_state, action, new_q)

        # Simulation ended (for any reason) - if goal position is achived - update Q value with reward directly
        if i == len(env.ask):
            done = True
            first = -1
            total_cost = -1
            total_avg_price = -1
        if posenv.getQuantity() == 0:
            
            deviation = np.std(desv_list)
            desv_list_aggregated.append(deviation)
            done = True
            if episode % 100 == 0:
                print(str(epsilon) + " " + str(deviation))

            if episode % 1000 == 0:
                s.Save(env, "data", "environment-POV-" + symbol)
                s.Save(posenv, "data", "positionenvironment-POV-" + symbol)
                np.savetxt('data/desv_list-POV-' + symbol + '.txt', desv_list_aggregated, delimiter=';')
                print(str(episode) + "=> saved")
        discrete_state = new_discrete_state
    


np.savetxt('data/desv_list-POV-' + symbol + '.txt', desv_list_aggregated, delimiter=';')
s.Save(env, "data", "environment-POV-" + symbol)
s.Save(posenv, "data", "positionenvironment-POV-" + symbol)
print("-----Fin------")
