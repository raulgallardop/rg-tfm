import numpy as np
from LoadData_Keras import Environment
from LoadData_Keras import PositionEnv
from LoadData_Keras import DQN
from LoadData_Keras import Saver
import glob
import tensorflow as tf
from tensorflow import keras
from tqdm import tqdm
import datetime
import sys
import os


symbol = "010005SAP"

if len(sys.argv) > 1:
    print(sys.argv[1])
    symbol = sys.argv[1]
BASE_PATH = "../AssetData/"+ symbol + "-clean/"
FILE_LIST = glob.glob(BASE_PATH + "*csv")
FILE_LIST.sort()
DISCRETE_WIN_SIZE = 20
NUM_FEATURES = 2
INITIAL_POSITION = 100000
POV_PRC = 20
PRC_PERMANENT_IMPACT = 0

env = Environment(FILE_LIST, "", False, file_surname="-" + symbol, size=DISCRETE_WIN_SIZE)
saver = Saver()
if os.path.isfile('data/positionenvironment-POV-Keras.pkl'):
    print("Load")
    posenv = saver.Load("data", "positionenvironment-POV-Keras")

else:
    print("Not Load")
    posenv = PositionEnv(INITIAL_POSITION, POV_PRC)

LEARNING_RATE = 0.1

DISCOUNT = 0.95
SHOW_EVERY = 50
STATS_EVERY = 100
EPISODES = 1500


# Deep Q Part Definition
keras.backend.clear_session()
tf.random.set_seed(10)
np.random.seed(10)
gamma = 0.95
copy_step = 25
num_actions = 5
max_experiences = 20000
min_experiences = 100
batch_size = 16 
lr = 1e-2


current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
log_dir = 'logs/VWAP/' + current_time + '-' + str(gamma) + '-' + str(lr) + '-' + str(EPISODES) + '-' + symbol
summary_writer = tf.summary.create_file_writer(log_dir)


TrainNet = DQN(num_actions, NUM_FEATURES, gamma, max_experiences, min_experiences, batch_size, lr)
TargetNet = DQN(num_actions, NUM_FEATURES, gamma, max_experiences, min_experiences, batch_size, lr)
if os.path.isfile("data/Train.h5"):
    TrainNet.LoadModel("data/Train")
    TargetNet.LoadModel("data/Target")


# End of Deep Q Part Definition

# Exploration settings
epsilon = 1  # not a constant, qoing to be decayed
START_EPSILON_DECAYING = 1
END_EPSILON_DECAYING = EPISODES // 2
epsilon_decay_value = epsilon/(END_EPSILON_DECAYING - START_EPSILON_DECAYING)
permanent_shift = 0

desv_list = []
desv_list_aggregated = []
for episode in tqdm(range(EPISODES)):
    cumulative_volume = 0
    target_volume = 0
    episode_reward = 0
    i = np.random.randint(len(env.ask))
    first = env.getFirstAskPrice(i)
    total_cost = 0
    total_avg_price = 0
    done = False
    discrete_state = posenv.getDiscreteState()
    posenv.reset(INITIAL_POSITION)
    desv_list = []
    portfolio_volatility = []
    portfolio_initial_value = first * INITIAL_POSITION
    epsilon -= epsilon_decay_value


    # time reference to compare the VWAP change
    time = -1
    #time reference # bars liquidation
    barduration = 0
    if episode % SHOW_EVERY == 0:
        render = True
        print(episode, " ", epsilon, " ", np.mean(desv_list_aggregated[-min(100, episode):]), " ", permanent_shift)
    else:
        render = False
    losses = list()
    permanent_shift = 0
    iter = 0
    while not done and i < len(env.ask)-1:
        
        liquidation = posenv.liquidation_state
        prc_liquidation = posenv.getQuantity() / INITIAL_POSITION
        #time_position = posenv.getTimePosition(env.getTime(i))
        s = [liquidation, prc_liquidation]
        action = TrainNet.get_action(s, epsilon)

        # new_state es el estado del siguiente tick
        # reward es el coste
        # done es si ha terminado
        quantity = posenv.step(action)

        posenv.updatePOVVolume(env.getVolume(i))
        cost, inventory, avg_price = env.getcost(quantity, i)
        posenv.liquidate(quantity - inventory)
        if avg_price > -1:
            if np.random.random() < PRC_PERMANENT_IMPACT:
                permanent_shift += avg_price - env.getFirstAskPrice(i)
            total_avg_price += (avg_price + permanent_shift) * (quantity - inventory) / INITIAL_POSITION

        cumulative_volume += env.getVolume(i)

        i+=1
        iter+=1

        # calculation portfolio volatility as remaining quantity volatility in %
        portfolio_volatility.append((env.getFirstAskPrice(i) + permanent_shift - first) \
                                    / first * posenv.getQuantity() / INITIAL_POSITION)

        # we have updated the POV Volume and the liquidated inventory
        new_discrete_state = posenv.getDiscreteState()

        cost =posenv.getLiquidationState()

       

        desv_list.append(abs(posenv.getLiquidationState()))
        liquidation = posenv.liquidation_state
        prc_liquidation = posenv.getQuantity() / INITIAL_POSITION
        s2 = [liquidation, prc_liquidation]
        exp = {'s': s, 'a': action, 'r': abs(cost), 's2': s2} 
        TrainNet.add_experience(exp)
        loss = TrainNet.train(TargetNet)
        losses.append(loss)

        if iter % copy_step == 0:
            TargetNet.copy_weights(TrainNet)

        if i == len(env.ask):
            done = True
            first = -1
            total_cost = -1
            total_avg_price = -1
        if posenv.getQuantity() == 0:

            deviation = np.std(desv_list)
            desv_list_aggregated.append(deviation)
            done = True
            if episode % SHOW_EVERY == 0:
                # save the objects
                saver.Save(env, "data", "environment-POV-keras-" + symbol)
                saver.Save(posenv, "data", "positionenvironment-POV-keras-" + symbol)
                TrainNet.saveModel("Train-VWAP-" + symbol)
                TargetNet.saveModel("Target-VWAP-" + symbol)
                print("Saved => " + str(deviation))

        discrete_state = new_discrete_state
    avg_rewards = np.mean(desv_list[max(0, -SHOW_EVERY):])
    with summary_writer.as_default():
        tf.summary.scalar('episode reward', np.std(desv_list), step=episode)
        tf.summary.scalar('running avg reward(100)', avg_rewards, step=episode)
        tf.summary.scalar('length', len(desv_list), step=episode)
        diff_avg_price = total_avg_price + permanent_shift - first
        if posenv.getQuantity() > 0:
            diff_avg_price = -1
        tf.summary.scalar('Price Distance', diff_avg_price / first * 100, step = episode)
        tf.summary.scalar('Portfolio Deviation', np.std(portfolio_volatility) * 100, step=episode)


np.savetxt('data/desv_list-POV-keras-' + symbol + '.txt', desv_list_aggregated, delimiter=';')

TrainNet.saveModel("Train-POV-keras-" + symbol)
TargetNet.saveModel("Target-POV-keras" + symbol)
# save the objects
saver.Save(env, "data", "environment-POV-keras-" + symbol)
saver.Save(posenv, "data", "positionenvironment-POV-keras-" + symbol)

print("-----Fin------")
