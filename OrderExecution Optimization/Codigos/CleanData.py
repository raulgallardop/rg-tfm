import pandas as pd
import numpy as np
import datetime
import glob
import tqdm
import os
import sys


symbol = "010005ALV"

if len(sys.argv) > 1:
    print(sys.argv[1])
    symbol = sys.argv[1]

INPUT_PATH = "../AssetData/"+symbol+"-filtered/"
OUTPUT_PATH = "../AssetData/"+symbol+"-clean/"

if not os.path.isdir(OUTPUT_PATH):
    os.mkdir(OUTPUT_PATH)
    print("Creating folder: " + OUTPUT_PATH)

FILE_LIST = glob.glob(INPUT_PATH + "*csv")
print(FILE_LIST)
MIN_SECONDS = 10  # 10 seconds between new ticks


def cleanBidAsk(data, minmove):
    data["Bid1"] = data["Bid2"] + minmove
    data["Ask1"] = data["Ask2"] - minmove
    data["Midprice"] = (data["Ask1"] + data["Bid1"]) / 2
    return data

header = "TimeStamp;Time;Open;High;Low;Close;Volume;Bid1;Bid2;Bid3;Bid4;Bid5;Bid6;"
header += "Bid7;Bid8;Bid9;Bid10;Ask1;Ask2;Ask3;Ask4;Ask5;Ask6;Ask7;Ask8;Ask9;"
header += "Ask10;BidVol1;BidVol2;BidVol3;BidVol4;BidVol5;BidVol6;BidVol7;"
header += "BidVol8;BidVol9;BidVol10;AskVol1;AskVol2;AskVol3;AskVol4;AskVol5;"
header += "AskVol6;AskVol7;AskVol8;AskVol9;AskVol10;"
header += "BidOrders1;BidOrders2;BidOrders3;BidOrders4;"
header += "BidOrders5;BidOrders6;BidOrders7;BidOrders8;BidOrders9;BidOrders10;"
header += "AskOrders1;AskOrders2;AskOrders3;AskOrders4;AskOrders5;AskOrders6;"
header += "AskOrders7;AskOrders8;AskOrders9;AskOrders10"


for file in tqdm.tqdm(FILE_LIST):
    file_name = file.split("/")[-1]
    data = pd.read_csv(file, sep=';', header=0, names=header.split(";"))
    data = cleanBidAsk(data, 0.01)
    data = data.replace(2147483647, np.nan).dropna()
    data.reset_index(drop=True, inplace=True)

    #Time is the data from the Bar, timestamp is the computer timestamp
    data["Time"] =pd.to_datetime(data['Time'])
    data["TimeDiff"] = data["Time"].diff()

    # at the volume there is information from the bar during it construction
    # if we use volume every tick we will use the same volume as many times
    # as new ticks we found
    data["VolDiff"] = data["Volume"].diff()
    data = data[data.TimeDiff.notnull()]

    #we have to delete duplicated row (voldiff == 0)
    data = data[data.VolDiff != 0]
    counter = datetime.timedelta(seconds=0)
    df = data

    for index, row in data.iterrows():
    
        if row["VolDiff"] < 0:
            
            df._set_value(index, ["VolDiff"], row["Volume"])


    for index, row in data.iterrows():
        counter += row.TimeDiff
        if counter < datetime.timedelta(seconds=MIN_SECONDS):
            df = df.drop(index=index)
        else:
            counter = datetime.timedelta(seconds=0)
    if len(df) > 0:
        df.reset_index(drop=True, inplace=True)
        df["CumVol"] = df["VolDiff"].cumsum()
        df["DiffVol"] = df["CumVol"].diff()
        df["DiffVol"][0] = df["CumVol"][0]
        df = df.drop(columns=["TimeDiff", "CumVol", "DiffVol"])
        df.to_csv(OUTPUT_PATH + file_name, header=None, sep=";", index=False)
        print(file_name)

print("-----------Fin---------")
