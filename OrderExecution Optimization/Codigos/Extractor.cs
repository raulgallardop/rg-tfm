#region usings
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Security;

using Plugins;
using Plugins.Indicator;
using Plugins.Strategy;

using Tengoku.Kumo.Calc.Indicators;
using Tengoku.Kumo.Charting.Chart;
using Tengoku.Kumo.Charting.Frame;
using Tengoku.Kumo.Charting.Scale;

using Tengoku.VisualChart.Plugins.Attributes;
using Tengoku.VisualChart.Plugins.Types;
using VisualChart.Development.Runtime.DataSeries;
using VisualChart.Development.Runtime.Plugins;
using System.Reflection;
using System.Linq;
using System.IO;
#endregion

namespace DataExporter
{
    /// <summary>
    /// Your indicator description here.
    /// </summary>
    [Indicator(Name = "DataExporter", Description = "Your indicator description here")]
    public class DataExporter : IndicatorPlugin
    {
        // Parameter format example
        /// <summary>
        /// Your parameter description here.
        /// </summary>
        [Parameter(Name = "BucketSize", DefaultValue = 500, MinValue = 2, MaxValue = 100, Step = 1)]
        private long BucketSize;
        [Parameter(Name = "IniPath", DefaultValue = "c:\\SecurityData\\")]
        private string inipath;
        public class SecurityData
        {
            public DateTime TimeStamp { get; set; }
            public DateTime Time { get; set; }
            public double Open { get; set; }
            public double High { get; set; }
            public double Low { get; set; }
            public double Close { get; set; }
            public double Volume { get; set; }
            public List<double> BidPrices{ get; set; }
            public List<double> AskPrices { get; set; }
            public List<double> BidVolume { get; set; }
            public List<double> AskVolume { get; set; }
            public List<double> BidOrder { get; set; }
            public List<double> AskOrder { get; set; }

        }
        // Indicator declaration sample
        // AvSimple avSimple;
        SecurityData s;
        List<SecurityData> listofdata;
        long lastdate;
        long tick = 0;
        string path, symbolname;
      
        /// <summary>
        /// This method is used to configure the indicator and is called once before any indicator method is called.
        /// </summary>
        public override void OnInitCalculate()
        {
            s = new SecurityData();
            listofdata = new List<SecurityData>();
            
            symbolname = this.Data.GetSymbolInfo(SymbolInfo.Code).ToString();
            lastdate = Date();
            path = inipath + "\\" + symbolname;
            if (!Directory.Exists(path)){
                Directory.CreateDirectory(path);
            }
                
        }

        /// <summary>
        /// Called on each bar update event.
        /// </summary>
        /// <param name="bar">Bar index.</param>
        public override void OnCalculateBar(int bar)
        {
            s = getSecurityData();

            if (lastdate == Date()) listofdata.Add(s);

            path = inipath + "\\" + symbolname + "\\" + symbolname + "-" + 
                Date() + ".csv";
            

            if (listofdata.Count > BucketSize || lastdate != Date())
            {
                PrintData(path, listofdata, "");
                listofdata.Clear();
            }
            if (lastdate != Date())
            {
                listofdata.Add(s);
                lastdate = Date();
            }
            
        }
        private SecurityData getSecurityData()
        {
            
            SecurityData s = new SecurityData();
            s.Close = Close();
            s.High = High();
            s.Low = Low();
            s.Open = Open();
            s.Volume = Volume();
            s.Time = TimeEx(ref tick, 0);
            s.BidPrices = getPrices(OrderSide.Buy);
            s.AskPrices = getPrices(OrderSide.Sell);
            s.BidVolume = getVolume(OrderSide.Buy);
            s.AskVolume = getVolume(OrderSide.Sell);
            s.BidOrder = getOrders(OrderSide.Buy);
            s.AskOrder = getOrders(OrderSide.Sell);
            s.TimeStamp = DateTime.Now;
            return s;
        }
        private string getHeader<T>()
        {
            string s =" ";

            return s;
        }
        private string getData(SecurityData d)
        {
            string s;
            s = d.TimeStamp.ToString("dd/MM/yyyy HH:mm:ss") + ";" + d.Time + ";" + d.Open + ";" + d.High + ";" + d.Low + ";" + 
                d.Close + ";" + d.Volume;
            foreach (double data in d.BidPrices)
            {
                s += ";" + data;
            }
            foreach (double data in d.AskPrices)
            {
                s += ";" + data;
            }
            foreach (double data in d.BidVolume)
            {
                s += ";" + data;
            }
            foreach (double data in d.AskVolume)
            {
                s += ";" + data;
            }
            foreach (double data in d.BidOrder)
            {
                s += ";" + data;
            }
            foreach (double data in d.AskOrder)
            {
                s += ";" + data;
            }
            return s;
        }
        private void PrintData(string path, List<SecurityData> l, string header)
        {
            
            

            using (var writer = new StreamWriter(path, append: true))
            {
                if (!File.Exists(path))
                    writer.WriteLine(header);

                foreach (var item in l)
                {
                    string d = getData(item);
                    writer.WriteLine(d);
                }
            }
        }
        public List<double> getPrices(OrderSide side)
        {
            List<double> lista = new List<double>();
            double field = 0;
            if (side == OrderSide.Buy)
            {
                field = Convert.ToDouble(GetFeedFields(FeedFields.Buy1));
                lista.Add(field);
            }
            else
            {
                field = Convert.ToDouble(GetFeedFields(FeedFields.Sell1));
                lista.Add(field);
            }
            for(int i = 2; i <= 10; i++)
            {
                lista.Add(LimitPrice(i, side));
            }

            return lista;
        }
        public List<double> getVolume(OrderSide side)
        {
            List<double> lista = new List<double>();
            for (int i = 1; i <= 10; i++)
            {
                lista.Add(LimitVol(i, side));
            }

            return lista;
        }
        public List<double> getOrders(OrderSide side)
        {
            List<double> lista = new List<double>();
            for (int i = 1; i <= 10; i++)
            {
                lista.Add(LimitOrder(i, side));
            }

            return lista;
        }
        public void WriteCSV<T>(IEnumerable<T> items, string path)
        {
            Type itemType = typeof(T);
            var props = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                .OrderBy(p => p.Name);

            using (var writer = new StreamWriter(path, append: true))
            {
                if (!File.Exists(path))
                    writer.WriteLine(string.Join(";", props.Select(p => p.Name)));

                foreach (var item in items)
                {
                    writer.WriteLine(string.Join(";", props.Select(p => p.GetValue(item, null))));
                }
            }
        }

        #region Visual Chart Code

        /// <summary>
        /// Performs calculus between startBar and endBar.
        /// </summary>
        /// <param name="startBar">Initial calculus bar.</param>
        /// <param name="endBar">End calculus bar.</param>
        public override void OnCalculateRange(int startBar, int endBar)
        {
            int i = this.StartBar;
            if (startBar > i)
                i = startBar;

            while (!this.ShouldTerminate && i <= endBar)
            {
                this.CurrentBar = i;
                this.CalculateAggregators();
                this.OnCalculateBar(i);
                i++;
            }
        }

        /// <summary>
        /// Sets calculus parameters.
        /// </summary>
        /// <param name="paramList">Parameters list.</param>
        public override void OnSetParameters(List<object> paramList)
        {
            BucketSize = Convert.ToInt32(paramList[0]);
            inipath = Convert.ToString(paramList[1]);
        }

        /// <summary>
        /// This function is used to create the data series corresponding to any indicator and to obtain an identifier of this series. To do so, we need to declare first a variable DataIdentifier type. Once the variable is defined we will always assign to it the the value of the function GetIndicatorIdentifier in order to create the indicator data series and to obtain an identifier of the same indicator. The identifier of the indicator must be obtained from the procedure <see cref="OnInitCalculate"/>.
        /// Later on, in order to obtain the value of an indicator we must use the functionGetIndicatorValue and indicate in the parameter Data the variable on which we have saved the value of the corresponding indicator. The identifier obtained by this function can be use don any .NET function on which a Data is required (Data series on which the different functions are calculated).
        /// </summary>
        /// <param name="indicator">Indicator Id.</param>
        /// <param name="parentDataIdentifier">Identifier of the series on which the indicator is calculated. If we set this parameter as data, we will be calculating the indicator on the data or series on which the strategy is being calculated. If we are willing to obtain the identifier of an indicator being calculated on another indicator we shall indicate within this parameter the identifier of the indicator we are willing to use as calculation.</param>
        /// <param name="optionalParameters">Indicator parameters (can be null).</param>
        /// <returns>Indicator source identifier.</returns>        
        public DataIdentifier GetIndicatorIdentifier(Indicators indicator, DataIdentifier parentDataIdentifier, params object[] optionalParameters)
        {
            return base.GetIndicatorIdentifier((long)indicator, parentDataIdentifier, optionalParameters);
        }

        /// <summary>
        /// This function is used to create the data series corresponding to any indicator and to obtain an identifier of this series. To do so, we need to declare first a variable DataIdentifier type. Once the variable is defined we will always assign to it the the value of the function GetIndicatorIdentifier in order to create the indicator data series and to obtain an identifier of the same indicator. The identifier of the indicator must be obtained from the procedure <see cref="OnInitCalculate"/>.
        /// Later on, in order to obtain the value of an indicator we must use the functionGetIndicatorValue and indicate in the parameter Data the variable on which we have saved the value of the corresponding indicator. The identifier obtained by this function can be use don any .NET function on which a Data is required (Data series on which the different functions are calculated).
        /// </summary>
        /// <param name="indicator">Indicator Id.</param>
        /// <param name="parentDataIdentifier">Identifier of the series on which the indicator is calculated. If we set this parameter as data, we will be calculating the indicator on the data or series on which the strategy is being calculated. If we are willing to obtain the identifier of an indicator being calculated on another indicator we shall indicate within this parameter the identifier of the indicator we are willing to use as calculation.</param>
        /// <param name="optionalParameters">Indicator parameters (can be null).</param>
        /// <returns>Indicator source identifier.</returns> 
        public DataIdentifier GII(Indicators indicator, DataIdentifier parentDataIdentifier, params object[] optionalParameters)
        {
            return base.GII((long)indicator, parentDataIdentifier, optionalParameters);
        }

        /// <summary>
        /// This function enables to obtain internally, the information of a certain system. This way, we can extract the information from this system without having to calculate it once and once again.
        /// </summary>
        /// <param name="strategy">Strategy id.</param>
        /// <param name="parentDataIdentifier">Identifier of the series on which the system is calculated. If we set this parameter as data, we will be calculating the indicator on the data or series on which the strategy is being calculated. If we are willing to obtain the identifier of an indicator being calculated on another indicator we shall indicate within this parameter the identifier of the indicator we are willing to use as calculation.</param>
        /// <param name="optionalParameters">System parameters (can be null).</param>
        /// <returns>system source identifier.</returns>
        public DataIdentifier GetSystemIdentifier(Strategies strategy, DataIdentifier parentDataIdentifier, params object[] optionalParameters)
        {
            return base.GetSystemIdentifier((long)strategy, parentDataIdentifier, optionalParameters);
        }

        /// <summary>
        /// This function enables to obtain internally, the information of a certain system. This way, we can extract the information from this system without having to calculate it once and once again.
        /// </summary>
        /// <param name="strategy">Strategy id.</param>
        /// <param name="parentDataIdentifier">Identifier of the series on which the system is calculated. If we set this parameter as data, we will be calculating the indicator on the data or series on which the strategy is being calculated. If we are willing to obtain the identifier of an indicator being calculated on another indicator we shall indicate within this parameter the identifier of the indicator we are willing to use as calculation.</param>
        /// <param name="optionalParameters">System parameters (can be null).</param>
        /// <returns>system source identifier.</returns>
        public DataIdentifier GSYSI(Strategies strategy, DataIdentifier parentDataIdentifier, params object[] optionalParameters)
        {
            return base.GSYSI((long)strategy, parentDataIdentifier, optionalParameters);
        }

        /// <summary>
        /// This function is used to paint the background of the window, for a certain bar, in the indicated color.
        /// </summary>    
        /// <param name="barsAgo">Number of bars backwards.The value 0 refers to the current bar.</param>
        /// <param name="color">Color used for the background on the given bar(BarsAgo).</param>
        public void SetBackgroundColor(int barsAgo, Color color)
        {
            this.SetBackgroundColor(barsAgo, Color.FromArgb(0, color.B, color.G, color.R).ToArgb());
        }

        /// <summary>
        /// Assigns the background color to the window of an indicator.
        /// </summary>
        /// <param name="color">Use of the function RGB to identify the background color of a window.</param>
        public void SetWndBackgroundColor(Color color)
        {
            this.SetWndBackgroundColor(Color.FromArgb(0, color.B, color.G, color.R).ToArgb());
        }

        /// <summary>
        /// This function assigns to the indicated bar of a certain indicator line, a certain color.
        /// </summary>
        /// <param name="barsAgo">Number of bars backwards.The value 0 refers to the current bar.</param>
        /// <param name="line">Identifies the data line to which the bar on which the property is established belongs.</param>
        /// <param name="color">Color to paint the indicated bar.</param>
        public void SetBarColor(int barsAgo, int line, Color color)
        {
            this.SetBarColor(barsAgo, line, Color.FromArgb(0, color.B, color.G, color.R).ToArgb());
        }

        /// <summary>
        /// This function assigns to the indicated bar, of a certain line of the indicator, the color, width and type of line and also the representation used in the rest of the parameters.
        /// </summary>
        /// <param name="barsAgo">Number of bars backwards.The value 0 refers to the current bar.</param>
        /// <param name="line">Identifies the data line to which the bar on which the property is established belongs.</param>
        /// <param name="color">Color to paint the indicated bar. FunctionRGB.</param>
        /// <param name="width">Indicates the width to be applied to the bar (1,2,..).</param>
        /// <param name="style">Style used for the representation: <see cref="LineStyle.Solid"/>-> continuous line <see cref="LineStyle.Dash"/>-> non-continuous line <see cref="LineStyle.Dot"/>-> dotted line <see cref="LineStyle.Dashdot"/>-> dotted line with point <see cref="LineStyle.Dashdotdot"/>-> dotted line with 2 points.</param>
        /// <param name="representation">Type or representation bo be used: <see cref="IndicatorRepresentation.Bars"/>-> bars <see cref="IndicatorRepresentation.Candlestic"/>-> candlesticks <see cref="IndicatorRepresentation.DottedLine"/>-> dotted line <see cref="IndicatorRepresentation.FilledHistogram"/>-> filled histogram <see cref="IndicatorRepresentation.Histogram"/>-> histogram <see cref="IndicatorRepresentation.Lineal"/>Lineal-> lineal <see cref="IndicatorRepresentation.Parabolic"/>-> Parabolic <see cref="IndicatorRepresentation.Volume"/>-> volume.</param>
        public void SetBarProperties(int barsAgo, int line, Color color, int width, LineStyle style, IndicatorRepresentation representation)
        {
            this.SetBarProperties(barsAgo, line, Color.FromArgb(0, color.B, color.G, color.R).ToArgb(), width, style, representation);
        }

        #endregion
    }
}
